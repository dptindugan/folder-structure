
export const fileChecker = (name, allowPdf) => {
  const imgPattern = /\b(jpe?g|png|gif|bmp)$/i;
  const documentPattern = /\b(jpe?g|png|gif|bmp|pdf)$/i;

  const pattern = allowPdf ? documentPattern : imgPattern;
  const regex = new RegExp(pattern);
  return regex.test(name);
};
