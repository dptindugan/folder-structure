import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div className='App'>
      <Router>
        <Switch>
          <Route exact path='/' component={Home} product='ade' />
          <Route
            exact
            path='/about-us/who-we-are'
            component={About}
            product='ade'
          />
          <Route exact path='/services' component={Services} product='ade' />
          <Route
            exact
            path='/terms-and-conditions'
            component={TermsAndConditionsMain}
            product='ade'
          />
          <Route
            exact
            path='/buyer-terms-and-conditions'
            component={BuyerTermsAndConditions}
            product='ade'
          />
          <Route
            exact
            path='/merchant-terms-and-conditions'
            component={MerchantTermsAndConditions}
            product='ade'
          />
          <Route
            exact
            path='/privacy-statement'
            component={PrivacyStatement}
            product='ade'
          />
          <Route
            exact
            path='/terms-of-use'
            component={TermsOfUse}
            product='ade'
          />
          <Route
            exact
            path='/about-us/approvals-and-recognition'
            component={AwardAndRecognition}
            product='ade'
          />
          <Route exact path='/about-us/careers' component={Career} />
          <Route
            exact
            path='/free-consultation'
            component={Consultation}
            product='ade'
          />
          <ProtectedRoute
            exact
            path='/aerotrade/marketplace'
            product='aerotrade'
            component={Marketplace}
          />
          <ProtectedRoute
            exact
            path='/aerotrade/marketplace/product/detail/:productId'
            product='aerotrade'
            component={ProductDetail}
          />
          <ProtectedRoute
            exact
            path='/aerotrade/marketplace/cart'
            product='aerotrade'
            component={Cart}
          />
          <ProtectedRoute
            exact
            path='/aerotrade/marketplace/checkout'
            product='aerotrade'
            component={Checkout}
          />
          <ProtectedRoute
            exact
            path='/aerotrade/marketplace/payment'
            product='aerotrade'
            component={Payment}
          />
          <ProtectedRoute
            exact
            path='/aerotrade/marketplace/success'
            product='aerotrade'
            component={SuccessPage}
          />
          <Route exact path='/contact' component={Contact} product='ade' />
          <GuestRoute exact path='/login' component={Login} product='ade' />
          <GuestRoute exact path='/signup' component={Login} product='ade' />
          <GuestRoute
            exact
            path='/forgot-password'
            product='ade'
            component={Forgotpassword}
          />
          <ProtectedRoute
            path='/v2/dashboard'
            product='ade'
            component={Dashboardv2}
          />
          <DashboardState>
            <OrderState>
              <ProtectedRoute
                path='/my-account'
                product='ade'
                component={Dashboard}
              />
            </OrderState>
          </DashboardState>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
